#include <iostream>
#include <ctime>

struct Task {

	char time[6];
};

int inputAmountOfTasks();
Task* arrayCreationByAmount(size_t sizeOfArray);
void randomArrayFilling(Task *arrayOfTasks, size_t sizeOfArray);
inline double conversionTimeIntoDecimal(Task time);
double findTotalTime(Task *arrayOfTasks, size_t sizeOfArray);
int findQuantityOfWorkingWeeks(int hours);
void findOverOneWorkingDayTasks(Task* tasks, size_t len);

int main() {

	size_t amount = inputAmountOfTasks();

	Task *tasksArray = arrayCreationByAmount(amount);

	randomArrayFilling(tasksArray, amount);

	double totalTime = findTotalTime(tasksArray, amount);

	std::cout << "Quantity of weeks to do all tasks: " << findQuantityOfWorkingWeeks(totalTime) << std::endl << std::endl;

	findOverOneWorkingDayTasks(tasksArray, amount);

	system("pause");
}

int inputAmountOfTasks() {

	size_t amount;
	std::cin >> amount;
	return amount;
}

Task* arrayCreationByAmount(size_t sizeOfArray) {

	return new Task[sizeOfArray];
}

void randomArrayFilling(Task *arrayOfTasks, size_t sizeOfArray) {

	srand(time(0));
	std::cout << "All tasks" << std::endl;

	for (size_t i = 0; i < sizeOfArray; i++)
	{
		arrayOfTasks[i].time[0] = (rand() % 10) + '0';
		arrayOfTasks[i].time[1] = (rand() % 10) + '0';
		arrayOfTasks[i].time[2] = ':';
		arrayOfTasks[i].time[3] = (rand() % 10) + '0';
		arrayOfTasks[i].time[4] = (rand() % 10) + '0';
		arrayOfTasks[i].time[5] = '\0';
		std::cout << "#" << i + 1 << " " << arrayOfTasks[i].time << std::endl;
	}
}

double conversionTimeIntoDecimal(Task time) {

	double numberOfTime = 0;
	numberOfTime += time.time[0] - '0';
	numberOfTime *= 10;
	numberOfTime += time.time[1] - '0';
	numberOfTime += (time.time[3] - '0') / 10.0;
	numberOfTime += (time.time[4] - '0') / 100.0;

	return numberOfTime;
}

double findTotalTime(Task *arrayOfTasks, size_t sizeOfArray) {

	double totalTime = 0;

	for (size_t i = 0; i < sizeOfArray; i++) {

		totalTime += conversionTimeIntoDecimal(arrayOfTasks[i]);
	}

	std::cout << "Total time is: " << totalTime << std::endl;

	return totalTime;
}

int findQuantityOfWorkingWeeks(int hours) {

	int weeks = hours / 40;

	if (hours % 40 != 0)
		weeks++;

	return weeks;
}

void findOverOneWorkingDayTasks(Task* tasks, size_t len) {

	bool flag = true;

	for (size_t i = 0; i < len; i++)
		if (conversionTimeIntoDecimal(tasks[i]) > 8.0) {

			if (flag)
				std::cout << "Task exceeding one day:\n";

			flag = false;

			std::cout << "#" << i + 1 << ": " << conversionTimeIntoDecimal(tasks[i]) << std::endl;
		}
}
